package com.itheima.reggie.interceptors;


import com.alibaba.fastjson.JSON;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 判断用户登录的拦截器
 */
@Slf4j
public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        //1.1 判断用户是否登录，获取session中的用户
        Long employeeId = (Long) request.getSession().getAttribute("employee");
        if (employeeId != null) {
            //将当前登录的员工id存入线程
            BaseContext.setCurrentId(employeeId);
            return true;
        }

        //1.2 判断用户是否登录，获取session中的用户
        Long userId = (Long) request.getSession().getAttribute("user");
        if (userId != null) {
            //将当前用户id存入线程
            BaseContext.setCurrentId(userId);
            return true;
        }

        //2. 用户未登录，响应code=0 && msg=NOTLOGIN
        log.info("用户未登录");
        R r = R.error("NOTLOGIN");
        String jsonString = JSON.toJSONString(r);
        response.getWriter().write(jsonString);
        return false;
    }

}
