package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.common.R;
import com.itheima.reggie.pojo.ShoppingCart;
import com.itheima.reggie.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("shoppingCart")
public class ShoppingCartController {


    @Autowired
    private ShoppingCartService shoppingCartService;

    /**
     * 查询用户的购物车
     */
    @GetMapping("list")
    public R list() {
        //1. 从当前线程中获取用户id
        Long userId = BaseContext.getCurrentId();
        //2. 根据用户id、查询购物车   select * from shopping_cart where user_id=?
        LambdaQueryWrapper<ShoppingCart> queryWrapper = Wrappers.lambdaQuery(ShoppingCart.class)
                .eq(ShoppingCart::getUserId, userId);
        List<ShoppingCart> shoppingCartList = shoppingCartService.list(queryWrapper);
        //3. 返回结果
        return R.success(shoppingCartList);
    }


    /**
     * 添加购物车
     */
    @PostMapping("add")
    public R add(@RequestBody ShoppingCart shoppingCart) {
        //1. 判断购物车是否包含菜品或者套餐    select * from shopping_cart where user_id=? and dish_id=?
        LambdaQueryWrapper<ShoppingCart> queryWrapper = Wrappers.lambdaQuery(ShoppingCart.class)
                .eq(ShoppingCart::getUserId, BaseContext.getCurrentId())
                .eq(shoppingCart.getDishId() != null, ShoppingCart::getDishId, shoppingCart.getDishId())
                .eq(shoppingCart.getSetmealId() != null, ShoppingCart::getSetmealId, shoppingCart.getSetmealId());
        ShoppingCart cart = shoppingCartService.getOne(queryWrapper);
        //2.判断 cart 对象是否为空
        if (cart != null) {
            //说明购物车包含商品， +1，更新操作
            cart.setNumber(cart.getNumber() + 1);
            shoppingCartService.updateById(cart);
        } else {
            //说明购物车不包含商品， 添加操作
            //设置用户id
            shoppingCart.setUserId(BaseContext.getCurrentId());
            //设置创建时间
            shoppingCart.setCreateTime(LocalDateTime.now());
            //设置数量 默认1
            shoppingCart.setNumber(1);
            shoppingCartService.save(shoppingCart);
        }
        return R.success("添加成功");
    }


    /**
     * 删除购物车的数据
     */
    @PostMapping("sub")
    public R sub(@RequestBody ShoppingCart shoppingCart) {
        //1. 根据用户id、提交dishId|setmealId，查询菜品|套餐，判断购物车商品的数量
        //select * from shopping_cart where user_id=? and dish_id=?
        LambdaQueryWrapper<ShoppingCart> queryWrapper = Wrappers.lambdaQuery(ShoppingCart.class)
                .eq(ShoppingCart::getUserId, BaseContext.getCurrentId())
                .eq(shoppingCart.getSetmealId() != null, ShoppingCart::getSetmealId, shoppingCart.getSetmealId())
                .eq(shoppingCart.getDishId() != null, ShoppingCart::getDishId, shoppingCart.getDishId());
        ShoppingCart cart = shoppingCartService.getOne(queryWrapper);
        if (cart.getNumber() > 1) {
            // 更新数据，减减
            cart.setNumber(cart.getNumber() - 1);
            shoppingCartService.updateById(cart);
        } else {
            // 删除数据
            // delete from shopping_cart where id=?
            shoppingCartService.removeById(cart.getId());
        }
        return R.success("删除成功");
    }

    /**
     * 清空购物车
     */
    @DeleteMapping("clean")
    public R clean() {
        //根据用户id删除
        LambdaQueryWrapper<ShoppingCart> queryWrapper = Wrappers.lambdaQuery(ShoppingCart.class)
                .eq(ShoppingCart::getUserId, BaseContext.getCurrentId());
        shoppingCartService.remove(queryWrapper);
        return R.success("清空成功");
    }

}
