package com.itheima.reggie.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.pojo.Employee;
import com.itheima.reggie.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * 员工管理模块
 */
@Slf4j
@RestController
@RequestMapping("employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    /**
     * 员工登录
     */
    @PostMapping("login")
    public R<Employee> login(@RequestBody Employee employee, HttpSession session) {

        //1. 获取请求参数：用户名，密码
        String username = employee.getUsername();
        String password = employee.getPassword();

        //2. 根据用户名，查询用户             select * from employee where username=?
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", username);
        Employee e = employeeService.getOne(queryWrapper);

        if (e == null) {
            return R.error("用户名不存在");
        }

        //3. 比对密码是否一致
        //对用户的输入密码进行加密
        password = DigestUtils.md5DigestAsHex(password.getBytes());
        if (!StringUtils.equals(password, e.getPassword())) {
            return R.error("密码错误");
        }


        //4. 判断用户状态是否启用
        if (e.getStatus() == 0) {
            return R.error("账号已被禁用");
        }


        //5. 登录成功，
        //5.1.将用户存入session
        session.setAttribute("employee", e.getId());
        //5.2 将用户信息响应给客户端
        return R.success(e);
    }


    /**
     * 员工退出
     */
    @PostMapping("logout")
    public R<String> logout(HttpSession session) {
        //1. 清理session，销毁
        session.invalidate();
        //2. 响应客户端数据
        return R.success("退出成功");
    }


    /**
     * 保存员工
     */
    @PostMapping
    public R save(@RequestBody Employee employee, HttpSession session) {
        //设置默认密码123456
        employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));
        //保存员工信息
        employeeService.save(employee);
        return R.success("添加成功");
    }


    /**
     * 分页查询
     */
    @GetMapping("page")
    public R page(@RequestParam(name = "page", required = true, defaultValue = "1") Integer page,
                  @RequestParam(name = "pageSize", required = true, defaultValue = "10") Integer pageSize,
                  @RequestParam(name = "name", required = false, defaultValue = "") String name) {
        //select * from employee where name like ? limit ?, ?
        //1. 创建分页对象
        Page<Employee> pageInfo = new Page<>(page, pageSize);
        //2. 创建条件对象
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(StringUtils.isNotBlank(name), "name", name);
        queryWrapper.orderByDesc("update_time");
        //3. 查询数据库
        employeeService.page(pageInfo, queryWrapper);
        //4. 返回数据
        return R.success(pageInfo);
    }


    /**
     * 启动/禁用员工账号
     */
    @PutMapping
    public R update(@RequestBody Employee employee, HttpSession session) {
        //更新数据库
        log.info(Thread.currentThread().getId() + "");
        employeeService.updateById(employee);
        return R.success("修改成功");
    }


    /**
     * 根据员工id查询
     */
    @GetMapping("{id}")
    public R findById( @PathVariable Long id ) {
        Employee employee = employeeService.getById(id);
        return  R.success(employee);
    }

}




