package com.itheima.reggie.controller;

import com.itheima.reggie.common.R;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * 处理文件上传下载
 */
@RestController
@RequestMapping("common")
@Slf4j
public class CommonController {

    @Value("${reggie.path}")
    private String basePath;

    /**
     * 文件上传
     */
    @PostMapping("upload")
    public R upload(MultipartFile file) throws IOException {
        //file是一个临时文件，需要转存到指定地方，否则方法执行完之后临时文件会被删除

        //1. 获取文件名
        String originalFilename = file.getOriginalFilename(); //abc.jpg

        //2. 判断文件夹是否存在
        File dist = new File(basePath);
        if (!dist.exists()) {
            //创建文件夹
            dist.mkdirs();
        }

        //3. 生产uuid唯一文件名        uuid_abc.jpg
        String uuidFilename = UUID.randomUUID().toString() + "_" + originalFilename;

        //4. 将文件存储到指定目录
        file.transferTo(new File(basePath + uuidFilename));

        return R.success(uuidFilename);
    }

    /**
     * 文件下载
     */
    @GetMapping("download")
    public void download(String name, HttpServletResponse response) throws Exception {
        //本质：输入流和输出流的对接
        FileInputStream is = new FileInputStream(basePath + name);
        ServletOutputStream os = response.getOutputStream();
        IOUtils.copy(is, os);

        is.close();
    }
}
