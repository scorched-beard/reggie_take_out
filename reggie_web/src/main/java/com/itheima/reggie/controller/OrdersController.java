package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.OrdersDto;
import com.itheima.reggie.pojo.OrderDetail;
import com.itheima.reggie.pojo.Orders;
import com.itheima.reggie.service.OrderDetailService;
import com.itheima.reggie.service.OrdersService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("order")
public class OrdersController {


    @Autowired
    private OrdersService ordersService;
    @Autowired
    private OrderDetailService orderDetailService;

    /**
     * 提交订单
     */
    @PostMapping("submit")
    public R submit(@RequestBody Orders orders) {
        ordersService.submit(orders);
        return R.success("下单成功");
    }


    /**
     * 移动端查询订单
     */
    @GetMapping("userPage")
    public R userPage(Integer page, Integer pageSize) {

        Page<Orders> pageInfo1 = new Page<>(page, pageSize);

        LambdaQueryWrapper<Orders> queryWrapper = Wrappers.lambdaQuery(Orders.class)
                .eq(Orders::getUserId, BaseContext.getCurrentId())
                .orderByDesc(Orders::getOrderTime);

        ordersService.page(pageInfo1, queryWrapper);

        //TODO: 开启数据拷贝
        Page<OrdersDto> pageInfo2 = new Page<>(page, pageSize);

        BeanUtils.copyProperties(pageInfo1, pageInfo2, "records");
        List<OrdersDto> ordersDtoList = new ArrayList<>();
        for (Orders orders : pageInfo1.getRecords()) {
            OrdersDto ordersDto = new OrdersDto();
            BeanUtils.copyProperties(orders, ordersDto);

            //查询订单详情数据
            LambdaQueryWrapper<OrderDetail> wrapper = Wrappers.lambdaQuery(OrderDetail.class)
                    .eq(OrderDetail::getOrderId, orders.getId());
            List<OrderDetail> orderDetailList = orderDetailService.list(wrapper);
            ordersDto.setOrderDetails(orderDetailList);

            //将每一个OrderDto放入List集合总
            ordersDtoList.add(ordersDto);
        }
        pageInfo2.setRecords(ordersDtoList);

        return R.success(pageInfo2);
    }

    /**
     * 后台系统查询订单
     */
    @GetMapping("page")
    public R page(Integer page, Integer pageSize, String number, String beginTime, String endTime) {

        Page<Orders> pageInfo = new Page<>(page, pageSize);

        LambdaQueryWrapper<Orders> queryWrapper = Wrappers.lambdaQuery(Orders.class)
                .eq(StringUtils.isNotBlank(number), Orders::getNumber, number)
                .between(StringUtils.isNotBlank(beginTime), Orders::getOrderTime, beginTime, endTime)
                .orderByDesc(Orders::getOrderTime);
        ordersService.page(pageInfo, queryWrapper);

        return R.success(pageInfo);
    }

}
