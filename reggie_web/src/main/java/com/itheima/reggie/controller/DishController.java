package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.pojo.Category;
import com.itheima.reggie.pojo.Dish;
import com.itheima.reggie.pojo.DishFlavor;
import com.itheima.reggie.service.CategoryService;
import com.itheima.reggie.service.DishFlavorService;
import com.itheima.reggie.service.DishService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("dish")
public class DishController {

    @Autowired
    private DishService dishService;
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 添加菜品
     */
    @PostMapping
    public R save(@RequestBody DishDto dishDto) {
        //保存菜品数据，以及口味数据
        dishService.saveWithFlavor(dishDto);
        //清理缓存
        String redisKey = "dishdata:categoryid:" + dishDto.getCategoryId();
        redisTemplate.delete(redisKey);
        return R.success("添加成功");
    }


    /**
     * 分页条件查询
     */
    @GetMapping("page")
    public R page(Integer page, Integer pageSize, String name) {
        return R.success(dishService.pageDishDto(page, pageSize, name));
    }

    /**
     * 根据菜品id查询菜品详情以及口味信息
     */
    @GetMapping("{dishId}")
    public R findById(@PathVariable Long dishId) {
        DishDto dishDto = dishService.getDishDtoById(dishId);
        return R.success(dishDto);
    }

    /**
     * 修改菜品信息
     */
    @PutMapping
    public R update(@RequestBody DishDto dishDto) {
        dishService.updateWithFlavor(dishDto);
        //清理缓存
        String redisKey = "dishdata:categoryid:" + dishDto.getCategoryId();
        Set keys = redisTemplate.keys("dishdata:categoryid:*");
        redisTemplate.delete(keys);
        return R.success("修改成功");
    }


    /**
     * 根据分类id查询菜品列表
     */
    @GetMapping("list")
    public R list(Long categoryId, String name) {
        //先查询redis的数据
        String redisKey = "dishdata:categoryid:" + categoryId;
        List<DishDto> redisData = (List<DishDto>) redisTemplate.opsForValue().get(redisKey);
        if (redisData != null) {
            return R.success(redisData);
        }

        // 调用业务层查询数据库数据
        List<DishDto> dishDtoList = dishService.listDishDto(categoryId, name);

        /*
         * 将数据存入redis中，key，value都会序列化
         *      key：string序列化方式
         *      value：JDK序列化方式，二进制数据，看的清楚？
         */
        redisTemplate.opsForValue().set(redisKey, dishDtoList);
        return R.success(dishDtoList);
    }

}
