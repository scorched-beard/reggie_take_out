package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.pojo.Category;
import com.itheima.reggie.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * 保存分类
     */
    @PostMapping
    public R save(@RequestBody Category category) {
        categoryService.save(category);
        return R.success("添加成功");
    }

    /**
     * 分页查询
     */
    @GetMapping("page")
    public R page(@RequestParam(name = "page", defaultValue = "1", required = true) Integer page,
                  @RequestParam(name = "pageSize", defaultValue = "10", required = true) Integer pageSize) {
        //1. 分页对象
        Page<Category> pageInfo = new Page<>(page, pageSize);

        //2. 条件对象
        QueryWrapper<Category> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("sort");//排序

        //3. 查询
        categoryService.page(pageInfo, queryWrapper);
        return R.success(pageInfo);
    }


    /**
     * 根基id删除分类
     */
    @DeleteMapping
    public R deleteById(Long id) {
        categoryService.remove(id);
        return R.success("删除成功");
    }

    /**
     * 修改分类信息
     */
    @PutMapping
    public R update(@RequestBody Category category) {
        categoryService.updateById(category);
        return R.success("修改成功");
    }


    /**
     * 根据type查询分类
     */
    @GetMapping("list")
    public R list(Integer type) {
        //select * from category where type=?
        //查询条件
        QueryWrapper<Category> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(type != null, "type", type);//type：1 2 不带
        queryWrapper.orderByAsc("sort");//排序

        //查询数据
        List<Category> list = categoryService.list(queryWrapper);
        return R.success(list);
    }

}
