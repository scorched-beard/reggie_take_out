package com.itheima.reggie.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.itheima.reggie.common.R;
import com.itheima.reggie.common.ValidateCodeUtils;
import com.itheima.reggie.pojo.User;
import com.itheima.reggie.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 发送短信
     */
    @PostMapping("sendMsg")
    public R sendMsg(@RequestBody User user, HttpSession session) {
        //1. 获取手机号
        String phone = user.getPhone();
        //2. 使用工具类生产一个随机4位数
        String code = ValidateCodeUtils.generateValidateCode(4).toString();
        //将验证码存入session中，下次登录的时候做对比
        //session.setAttribute("code", code);
        redisTemplate.opsForValue().set("code:" + phone, code, 120, TimeUnit.SECONDS);

        //3. 使用工具类发送短信
        //SMSUtils.sendMessage(phone, code);
        log.info("验证码已发送：{}", code);
        return R.success("短信发送成功");
    }


    /**
     * 用户登录
     */
    @PostMapping("login")
    public R login(@RequestBody Map<String, String> map, HttpSession session) {
        //1. 获取请求参数，session的验证码
        String code = map.get("code");
        String phone = map.get("phone");
        // String codeSession = (String) session.getAttribute("code");
        String codeSession = (String) redisTemplate.opsForValue().get("code:" + phone);

        //2. 判断验证码是否正确
        if (!StringUtils.equals(codeSession, code)) {
            return R.error("验证码错误");
        }

        //3. 判断用户是否新用户，根据手机号查询
        LambdaQueryWrapper<User> queryWrapper = Wrappers
                .lambdaQuery(User.class)
                .eq(User::getPhone, phone);
        User user = userService.getOne(queryWrapper);
        if (user == null) {
            //新用户注册
            user = new User();
            user.setPhone(phone); //设置手机号
            user.setStatus(1); //设置状态 1
            userService.save(user);
        }

        //4. 登录成功
        //删除验证码
        redisTemplate.delete("code:" + phone);
        //4.1 存入session
        session.setAttribute("user", user.getId());
        //4.2 返回user给客户端
        return R.success(user);

    }


}
