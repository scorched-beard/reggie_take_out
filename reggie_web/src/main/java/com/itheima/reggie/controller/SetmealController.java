package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.pojo.Category;
import com.itheima.reggie.pojo.Dish;
import com.itheima.reggie.pojo.Setmeal;
import com.itheima.reggie.pojo.SetmealDish;
import com.itheima.reggie.service.CategoryService;
import com.itheima.reggie.service.DishService;
import com.itheima.reggie.service.SetmealDishService;
import com.itheima.reggie.service.SetmealService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("setmeal")
public class SetmealController {

    @Autowired
    private SetmealService setmealService;
    @Autowired
    private DishService dishService;

    /**
     * 添加套餐
     */
    @PostMapping
    @CacheEvict(cacheNames = "setmealList", allEntries = true)
    public R save(@RequestBody SetmealDto setmealDto) {
        setmealService.saveWithDish(setmealDto);
        return R.success("添加成功");
    }

    /**
     * 分页条件查询
     */
    @GetMapping("page")
    public R page(Integer page, Integer pageSize, String name) {
        Page<SetmealDto> setmealDtoPage = setmealService.pageSetmealDto(page, pageSize, name);
        return R.success(setmealDtoPage);
    }

    /**
     * 批量删除
     */
    @DeleteMapping
    @CacheEvict(cacheNames = "setmealList", allEntries = true)
    public R deleteByIds(Long[] ids) {
        int count = setmealService.deleteByIds(ids);
        if (count == 0) {
            return R.error("套餐正在售卖，无法删除");
        }
        return R.success("成功删除" + count + "条数据");
    }

    /**
     * 批量停售、启售
     */
    @PostMapping("status/{status}")
    @CacheEvict(cacheNames = "setmealList", allEntries = true)
    public R updateStatus(@PathVariable Integer status, Long[] ids) {
        //update setmeal set status=? where id in ids
        LambdaUpdateWrapper<Setmeal> updateWrapper = Wrappers
                .lambdaUpdate(Setmeal.class)
                .set(Setmeal::getStatus, status) // 设置值
                .in(Setmeal::getId, ids);

        setmealService.update(updateWrapper);
        return R.success("修改成功");
    }


    /**
     * 根据分类id查询套餐列表
     */
    @GetMapping("list")
    @Cacheable(cacheNames = "setmealList", key = "#categoryId + ':' + #status", unless = "#result.data.size() == 0")
    public R<List> list(Long categoryId, Integer status) {
        //构造条件
        LambdaQueryWrapper<Setmeal> queryWrapper = Wrappers.lambdaQuery(Setmeal.class)
                .eq(Setmeal::getCategoryId, categoryId)
                .eq(Setmeal::getStatus, status);
        List<Setmeal> list = setmealService.list(queryWrapper);
        return R.success(list);
    }

    /**
     * 根据套餐id查询对应的菜品信息
     */
    @GetMapping("dish/{setmealId}")
    public R findDishBySetmealId(@PathVariable Long setmealId) {
        List<DishDto> dishDtoList = dishService.findDishBySetmealId(setmealId);
        return R.success(dishDtoList);
    }

}
