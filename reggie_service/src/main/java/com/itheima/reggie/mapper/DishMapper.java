package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.pojo.Dish;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DishMapper extends BaseMapper<Dish> {
    /**
     * 分页查询菜品详情数据
     *
     * @param page 分页对象，xml中可以从里面进行自动取值，传递参数Page就会自动分页，必须放在第一位
     * @param name 菜品名称，模糊查询
     * @return 分页结果
     */
    Page<DishDto> pageDishDto(@Param("page") Page<DishDto> page,
                              @Param("name") String name);

    List<DishDto> findDishBySetmealId(Long setmealId);

    //根据分类id查询菜品列表
    List<DishDto> listDishDto(@Param("categoryId") Long categoryId,
                              @Param("name") String name);

    //根据菜品id查询菜品详情以及口味信息
    DishDto getDishDtoById(Long dishId);
}
