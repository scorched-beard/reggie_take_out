package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.pojo.Setmeal;

public interface SetmealService extends IService<Setmeal> {
    /**
     * 保存套餐数据，以及对应的菜品数据
     * @param setmealDto
     */
    void saveWithDish(SetmealDto setmealDto);

    /**
     * 删除套餐数据，以及对应的菜品关系
     * @param ids
     */
    int deleteByIds(Long[] ids);

    Page<SetmealDto> pageSetmealDto(Integer page, Integer pageSize, String name);
}
