package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.pojo.Category;

public interface CategoryService extends IService<Category> {

    /**
     * 根据id删除分类，检查分类是否关联菜品或者套餐
     * @param id
     */
    void remove(Long id);
}
