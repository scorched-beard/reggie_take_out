package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.mapper.DishMapper;
import com.itheima.reggie.mapper.SetmealMapper;
import com.itheima.reggie.pojo.Setmeal;
import com.itheima.reggie.pojo.SetmealDish;
import com.itheima.reggie.service.SetmealDishService;
import com.itheima.reggie.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealService {

    @Autowired
    private SetmealDishService setmealDishService;

    @Override
    @Transactional
    public void saveWithDish(SetmealDto setmealDto) {
        //1. 保存套餐。操作setmeal表，添加一条数据
        this.save(setmealDto);

        //2. 保存菜品关系。操作setmeal_dish表，添加多条数据
        //2.1 设置每个SetmealDish的setmealId
        setmealDto.getSetmealDishes().forEach(setmealDish -> setmealDish.setSetmealId(setmealDto.getId()));
        //2.1 批量保存
        setmealDishService.saveBatch(setmealDto.getSetmealDishes());
    }

    /**
     * @param ids
     * @return 删除成功的条数
     */
    @Override
    public int deleteByIds(Long[] ids) {

        int count = 0;// 记录删除的条数

        //1. 遍历ids数组
        for (Long id : ids) {
            //2. 根据id删除套餐，判断套餐是否停售
            Setmeal setmeal = this.getById(id);
            if (setmeal.getStatus() == 1) {
                continue;
            }
            count++;
            //2.1 先删除套餐表            操作setmeal表
            this.removeById(id);
            //2.2 再删除套餐菜品关系表      操作setmeal_dish表   delete from setmeal_dish where setmeal_id=?
            LambdaQueryWrapper<SetmealDish> queryWrapper = Wrappers
                    .lambdaQuery(SetmealDish.class)
                    .eq(SetmealDish::getSetmealId, id);

            setmealDishService.remove(queryWrapper);

        }
        return count;
    }

    @Autowired
    private SetmealMapper setmealMapper;

    @Override
    public Page<SetmealDto> pageSetmealDto(Integer page, Integer pageSize, String name) {
        Page<SetmealDto> pageInfo = new Page<>(page, pageSize);
        setmealMapper.pageSetmealDto(pageInfo, name);
        return pageInfo;
    }
}
