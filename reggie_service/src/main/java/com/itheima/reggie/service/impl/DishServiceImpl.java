package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.mapper.DishMapper;
import com.itheima.reggie.pojo.Dish;
import com.itheima.reggie.pojo.DishFlavor;
import com.itheima.reggie.service.CategoryService;
import com.itheima.reggie.service.DishFlavorService;
import com.itheima.reggie.service.DishService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish> implements DishService {

    @Autowired
    private DishFlavorService dishFlavorService;

    @Override
    @Transactional
    public void saveWithFlavor(DishDto dishDto) {
        //1. 保存菜品数据，操作dish表，添加一条数据
        this.save(dishDto);

        //2. 保存口味数据，操作dish_flavor，添加多条数据
        List<DishFlavor> dishFlavorList = dishDto.getFlavors();
        dishFlavorList.forEach(dishFlavor -> dishFlavor.setDishId(dishDto.getId()));
        dishFlavorService.saveBatch(dishFlavorList);
    }

    @Override
    @Transactional
    public void updateWithFlavor(DishDto dishDto) {

        //1. 修改菜品数据，操作dish表，update dish set xxx=xxx where id=?
        this.updateById(dishDto);

        //2. 修改口味数据，操作dish_flavor，先删再加
        // delete from dish_flavor where dish_id=?
        QueryWrapper<DishFlavor> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("dish_id", dishDto.getId());
        dishFlavorService.remove(queryWrapper);
        List<DishFlavor> dishFlavorList = dishDto.getFlavors();
        //给每个口味设置dishId
        dishFlavorList.forEach(dishFlavor -> dishFlavor.setDishId(dishDto.getId()));
        dishFlavorService.saveBatch(dishFlavorList);
    }


    @Autowired
    private DishMapper dishMapper;

    @Override
    public Page<DishDto> pageDishDto(Integer page, Integer pageSize, String name) {
        Page<DishDto> pageInfo = new Page<>(page, pageSize);
        return dishMapper.pageDishDto(pageInfo, name);
    }

    @Override
    public List<DishDto> findDishBySetmealId(Long setmealId) {
        return dishMapper.findDishBySetmealId(setmealId);
    }

    //根据分类id查询菜品列表
    @Override
    public List<DishDto> listDishDto(Long categoryId, String name) {
        return dishMapper.listDishDto(categoryId, name);
    }

    //根据菜品id查询菜品详情以及口味信息
    @Override
    public DishDto getDishDtoById(Long dishId) {
        return dishMapper.getDishDtoById(dishId);
    }
}
