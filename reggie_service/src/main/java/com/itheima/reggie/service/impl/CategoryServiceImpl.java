package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.CustomerException;
import com.itheima.reggie.mapper.CategoryMapper;
import com.itheima.reggie.pojo.Category;
import com.itheima.reggie.pojo.Dish;
import com.itheima.reggie.pojo.Setmeal;
import com.itheima.reggie.service.CategoryService;
import com.itheima.reggie.service.DishService;
import com.itheima.reggie.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

    @Autowired
    private DishService dishService;

    @Autowired
    private SetmealService setmealService;


    /**
     * 根据id删除分类，检查分类是否关联菜品或者套餐
     *
     * @param id
     */
    @Override
    public void remove(Long id) {
        //1. 判断分类是否关联菜品 select count(*) from dish where category_id=?
        QueryWrapper<Dish> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("category_id", id);
        int count1 = dishService.count(queryWrapper1);
        if (count1 > 0) {
            throw new CustomerException("该分类不能删除，关联了其它菜品");
        }

        //2. 判断分类是否关联套餐 select count(*) from setmeal where category_id=?
        QueryWrapper<Setmeal> queryWrapper2 = new QueryWrapper<>();
        queryWrapper2.eq("category_id", id);
        int count2 = setmealService.count(queryWrapper2);
        if (count2 > 0) {
            throw new CustomerException("该分类不能删除，关联了其它套餐");
        }

        //3. 真正的删除分类的数据 delete from category where id=?
        super.removeById(id);
    }
}
