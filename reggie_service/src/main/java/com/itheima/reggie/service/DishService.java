package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.pojo.Dish;

import java.util.List;

public interface DishService extends IService<Dish> {
    /**
     * 保存菜品数据，以及菜品口味关系数据
     *
     * @param dishDto
     */
    void saveWithFlavor(DishDto dishDto);

    /**
     * 更新菜品数据，以及菜品口味数据
     *
     * @param dishDto
     */
    void updateWithFlavor(DishDto dishDto);

    Page<DishDto> pageDishDto(Integer page, Integer pageSize, String name);

    List<DishDto> findDishBySetmealId(Long setmealId);

    //根据分类id查询菜品列表
    List<DishDto> listDishDto(Long categoryId, String name);

    //根据菜品id查询菜品详情以及口味信息
    DishDto getDishDtoById(Long id);
}
