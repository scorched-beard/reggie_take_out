package com.itheima.reggie.common;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLIntegrityConstraintViolationException;

/**
 * 全局异常处理器
 */
@RestControllerAdvice
public class GlobalExceptionHandler {


    /**
     * 处理指定异常：SQLIntegrityConstraintViolationException
     */
    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public R handler1(SQLIntegrityConstraintViolationException ex) {
        String message = ex.getMessage();
        String username = message.split(" ")[2];
        return R.error(username + "已存在");
    }


    /**
     * 处理 CustomerException
     */
    @ExceptionHandler(CustomerException.class)
    public R handler1(CustomerException ex) {
        String message = ex.getMessage();
        return R.error(message);
    }





}
