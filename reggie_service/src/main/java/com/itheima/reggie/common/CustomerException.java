package com.itheima.reggie.common;

import lombok.Data;

/**
 * 自定义异常类
 */
@Data
public class CustomerException extends RuntimeException {

    public CustomerException(String msg) {
        super(msg);
    }
}
